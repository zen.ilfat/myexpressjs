// modules/product/routes/productRoutes.js
const express = require("express");
const productController = require("../controller/productController"); // Correct the path

// import { getAllDataProduct } from "../controller/productController";

const productRoute = express.Router();

// Define routes using the productController
productRoute.get("/", productController.getAllDataProduct);
productRoute.post('/create', productController.createNewProduct);
productRoute.put('/:id', productController.updateprod);
productRoute.delete('/:id', productController.deleteProd);

module.exports = productRoute;

// export default productRoute;
