// modules/product/controllers/productController.js
// const express = require('express');
const productModel = require("../models/productModels"); // Correct the path

exports.getAllDataProduct = async (req, res,next) => {
  try {
    let [products] = await productModel.getAllData();
    res.json({
      status: 200,
      message: 'Sucsess',
      data:products,
    })
  } catch (error) {
    res.status(500).send( {
      message: 'error',
      data:error,
  });
}
};

exports.createNewProduct = async (req,res,next) =>{
try{
  const { name, price, stok } = req.body;
  let product = await productModel.insertProduct( name, price, stok)
  res.json({
    status: 200,
    message: 'Sucsess',
    data:product,
  })
}catch (err){
  console.log(err);
  res.json( {
    message:err.message,
    data:err,
});
}
}

exports.updateprod = async (req,res,next) =>{
  try{
    const {name, price, stok } = req.body;
    const id = req.params.id;
    let product = await productModel.updateProduct(id, name, price, stok)
    res.json({
      status: 200,
      message: 'Sucsess Update Produk',
      data:product,
    })
  }catch (err){
    console.log(err);
    res.json( {
      message:err.message,
      data:err,
  });
  }
}
exports.deleteProd = async (req,res,next) =>{
  try{
    const id = req.params.id;
    let product = await productModel.deleteProduct(id)
    res.json({
      status: 200,
      message: 'Sucsess Delete Produk',
    })
  }catch (err){
    console.log(err);
    res.json( {
      message:err.message,
      data:err,
  });
  }
}
// Get all products
// router.get('/', async (req, res) => {
//   try {
//     const products = await productModel.getAllData();
//     console.log(products); // You can log or send the products as a response
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Create a new product
// router.post('/', async (req, res) => {
//   const { name, price, stok } = req.body;

//   try {
//     const productId = await productModel.createData(name, price, stok);
//     // res.status(201).json({ productId });
//     console.log(productId);
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Update a product by ID
// router.put('/:id', async (req, res) => {
//   const productId = req.params.id;
//   const { name, price, stok } = req.body;

//   try {
//     await productModel.updateProduct(productId, name, price, stok);
//     // res.json({ message: 'Product updated successfully' });
//   } catch (error) {
//     console.error(error);
//     // res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Delete a product by ID
// router.delete('/:id', async (req, res) => {
//   const productId = req.params.id;

//   try {
//     await productModel.deleteProduct(productId);
//     // res.json({ message: 'Product deleted successfully' });
//   } catch (error) {
//     console.error(error);
//     // res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// module.exports = router;
