const util = require("util");
const db = require("../../../conffig/db"); // Update the path
const userSortColumn = "qty"; // This could come from user input
const getdalldata = "SELECT * FROM product";

const productModel = {
  getAllData: async () => {
    return db.execute(getdalldata);
  },

  updateProduct: async (productId, name, price, stok) => {
    try {
      console.log(productId, name, price, stok);
      const [result] = await db.execute(
        `UPDATE product SET namaprod = '${name}', qty = '${price}', stok = '${stok}' WHERE id = ${productId}`
      );
      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },

  insertProduct: async (name, price, stok) => {
    try {
      const [result] = await db.execute(
        "INSERT INTO product (namaprod, qty, stok) VALUES (?, ?, ?)",
        [name, price, stok]
      );

      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },

  deleteProduct: async (productId) => {
    // try {
    //   const results = await query("DELETE FROM product WHERE id = ?", [
    //     productId,
    //   ]);
    //   return results;
    // } catch (error) {
    //   throw error;
    // }
    try {
      const [result] = await db.execute(
        "DELETE FROM product WHERE id = ?",
        [productId]
      );

      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },
};

module.exports = productModel;
