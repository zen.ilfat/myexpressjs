// modules/product/routes/userRoutes.js
const express = require("express");
const userController = require("../controler/userController"); // Correct the path

// import { getAllDataProduct } from "../controller/userController";

const userRoute = express.Router();

// Define routes using the userController
userRoute.get("/", userController.getAllDataUser);
userRoute.post('/create', userController.createNewUser);
userRoute.put('/:id', userController.updateUser);
userRoute.delete('/:id', userController.deleteUser);

module.exports = userRoute;

// export default userRoute;
