// modules/product/controllers/productController.js
// const express = require('express');

const userModel = require("../models/userModels");

exports.getAllDataUser = async (req, res,next) => {
  try {
    let [products] = await userModel.getAllData();
    res.json({
      status: 200,
      message: 'Sucsess',
      data:products,
    })
  } catch (error) {
    res.status(500).send( {
      message: 'error',
      data:error,
  });
}
};

exports.createNewUser = async (req,res,next) =>{
try{
  const { name,number,alamat } = req.body;
  let product = await userModel.insertUser( name,number,alamat)
  res.json({
    status: 200,
    message: 'Sucsess Create data User',
  })
}catch (err){
  console.log(err);
  res.json( {
    message:err.message,
    data:err,
});
}
}

exports.updateUser = async (req,res,next) =>{
  try{
    const {name,number,alamat } = req.body;
    const id = req.params.id;
    let product = await userModel.updateUser(id, name,number,alamat)
    res.json({
      status: 200,
      message: 'Sucsess Update Produk',
      data:product,
    })
  }catch (err){
    console.log(err);
    res.json( {
      message:err.message,
      data:err,
  });
  }
}
exports.deleteUser = async (req,res,next) =>{
  try{
    const id = req.params.id;
    let product = await userModel.deleteUser(id)
    res.json({
      status: 200,
      message: 'Sucsess Delete Produk',
    })
  }catch (err){
    console.log(err);
    res.json( {
      message:err.message,
      data:err,
  });
  }
}
// Get all products
// router.get('/', async (req, res) => {
//   try {
//     const products = await userModel.getAllData();
//     console.log(products); // You can log or send the products as a response
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Create a new product
// router.post('/', async (req, res) => {
//   const { name,number,alamat } = req.body;

//   try {
//     const productId = await userModel.createData(name,number,alamat);
//     // res.status(201).json({ productId });
//     console.log(productId);
//   } catch (error) {
//     console.error(error);
//     res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Update a product by ID
// router.put('/:id', async (req, res) => {
//   const productId = req.params.id;
//   const { name,number,alamat } = req.body;

//   try {
//     await userModel.updateProduct(productId, name,number,alamat);
//     // res.json({ message: 'Product updated successfully' });
//   } catch (error) {
//     console.error(error);
//     // res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// // Delete a product by ID
// router.delete('/:id', async (req, res) => {
//   const productId = req.params.id;

//   try {
//     await userModel.deleteProduct(productId);
//     // res.json({ message: 'Product deleted successfully' });
//   } catch (error) {
//     console.error(error);
//     // res.status(500).json({ error: 'Internal Server Error' });
//   }
// });

// module.exports = router;
