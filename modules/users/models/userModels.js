const db = require("../../../conffig/db"); // Update the path
const getdalldata = "SELECT * FROM user";

const userModel = {
  getAllData: async () => {
    return db.execute(getdalldata);
  },

  updateUser: async (id, name,number,alamat) => {
    try {
      const [result] = await db.execute(
        `UPDATE user SET  name= '${name}', number = '${number}', alamat = '${alamat}' WHERE id = ${id}`
      );
      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },

  insertUser: async (name,number,alamat) => {
    try {
      const [result] = await db.execute(
        "INSERT INTO user (name, number, alamat) VALUES (?, ?, ?)",
        [name,number,alamat]
      );

      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },

  deleteUser: async (userId) => {
    // try {
    //   const results = await query("DELETE FROM product WHERE id = ?", [
    //     productId,
    //   ]);
    //   return results;
    // } catch (error) {
    //   throw error;
    // }
    try {
      const [result] = await db.execute(
        "DELETE FROM user WHERE id = ?",
        [userId]
      );

      return result;
    } catch (error) {
      // Handle errors
      console.error("Error during insert:", error);
      throw error;
    }
  },
};

module.exports = userModel;
