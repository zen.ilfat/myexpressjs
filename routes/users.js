// users.js
const express = require("express");
const userRoute = require("../modules/users/routes/userRoute");
const router = express.Router();
// import productRoutes from "../modules/product/routes/productRoute";
// Include productRoutes for the / route
router.use("/", userRoute);

// export default router;
module.exports = router;
