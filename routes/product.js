// users.js
const express = require("express");
const router = express.Router();
var productRoutes = require("../modules/product/routes/productRoute"); // Correct the import path
// import productRoutes from "../modules/product/routes/productRoute";
// Include productRoutes for the / route
router.use("/", productRoutes);

// export default router;
module.exports = router;
