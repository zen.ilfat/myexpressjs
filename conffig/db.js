const mysql = require('mysql2');
const { env } = require('process');
const dotenv = require('dotenv');
dotenv.config();
const db = mysql.createPool({
   connectionLimit: 1000,
   host: process.env.DB_HOST,
   database: process.env.DB_DATABASE,
   user:process.env.DB_USERNAME,
   password:process.env.DB_PASSWORD,
 });
  
 db.getConnection((err, connection) => {
   if (err) {
     console.error('Database connection failed:', err.message +''+env.DB_PASSWORD);
   } else {
     console.log('Databases is on ');
   }
 });
 
module.exports = db.promise();
